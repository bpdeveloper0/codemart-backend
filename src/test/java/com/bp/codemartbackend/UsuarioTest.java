package com.bp.codemartbackend;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.bp.codemartbackend.domain.Usuario;

class UsuarioTest {

	@Test
	void firstTest() {
		String testVariable = "Jhonny Fly";
		Usuario usr = new Usuario();
		usr.setNome("Jhonny Fly");
	    assertEquals(testVariable, usr.getNome() );
	}

}
