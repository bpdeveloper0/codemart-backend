package com.bp.codemartbackend.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bp.codemartbackend.domain.Usuario;
import com.bp.codemartbackend.repositories.UsuarioRepository;


@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usrRepo;

	public Usuario findUserById(Integer id)  {
			Optional<Usuario> obj =  usrRepo.findById(id);
			return obj.orElse(null);
		}

}
