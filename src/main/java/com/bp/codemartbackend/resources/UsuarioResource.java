package com.bp.codemartbackend.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bp.codemartbackend.domain.Usuario;
import com.bp.codemartbackend.services.UsuarioService;

@RestController
@RequestMapping(value ="/usuario")
public class UsuarioResource {

	@Autowired
	private UsuarioService userService ;

	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	public ResponseEntity<?> findUserById(@PathVariable Integer id){
		Usuario userFindObject = userService.findUserById(id);
		if(userFindObject != null) {
			return ResponseEntity.ok().body(userFindObject);
		}else {
			//Didn't find the record in the database
			return ResponseEntity.ok().body("Didn't find the record in the database");
		}
	}

}
