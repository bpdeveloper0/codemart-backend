package com.bp.codemartbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodemartbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodemartbackendApplication.class, args);
	}

}
